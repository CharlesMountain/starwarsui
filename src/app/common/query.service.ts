import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from './logger.service';
import { Category } from '../searchbar/searchbar.component';

export class Thing {
  constructor(
    public name: string) {
  }
}

@Injectable({
  providedIn: 'root'
})
export class QueryService {

  constructor(private httpClient: HttpClient,
              private log: LoggerService ) {}

  getThing(c: Category, id: number): Observable<Thing> {
    this.log.log('QueryService', `Querying Category ` + c.title + ' Id ' + id);
    return this.httpClient.get<Thing>('https://swapi.co/api/' + c.title + '/' + id);
  }
}
