import { Component, OnInit } from '@angular/core';
import {LoggerService} from '../common/logger.service';
import {QueryService, Thing} from '../common/query.service';
import {FormsModule} from '@angular/forms';

export class Category {
  constructor(public id: number,
              public title: string) {}
}

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {

  categories: Array<Category> = [];
  selectedCategory: Category = new Category(1, 'people');
  selectedId = 1;
  categoryFound: Thing = new Thing('Empty');

  constructor(private log: LoggerService, private query: QueryService) {
    this.categories.push(new Category(1, 'people'));
    this.categories.push(new Category(2, 'planets'));
    this.categories.push(new Category(3, 'films'));
    this.categories.push(new Category(4, 'species'));
    this.categories.push(new Category(5, 'vehicles'));
    this.categories.push(new Category(6, 'starships'));
  }

  ngOnInit() {
      this.query.getThing(this.selectedCategory, this.selectedId).subscribe(response => {
        console.log(response.name);
        this.categoryFound.name = response.name; });
  }

  searchSite(f: FormsModule) {
    this.log.log('SearchBar', 'Searching Category ' + this.selectedCategory.title + ' Id ' + String(this.selectedId));
    this.query.getThing(this.selectedCategory, this.selectedId);
  }

}















