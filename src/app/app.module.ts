import { BrowserModule } from '@angular/platform-browser';
import {NgModule, Query} from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { HttpClientModule} from '@angular/common/http';
import {LoggerService} from './common/logger.service';
import {QueryService} from './common/query.service';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    SearchbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [LoggerService, QueryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
